$(document).ready(function () {
        giveMeAllPosts();

        //giveMeComments();
    }
);

function giveMeAllPosts() {
    $.ajax(
        {
            method: "GET",
            url: "/list",
            dataType: "json",
            contentType: "application/json",
            complete: function (response) {

                $('#post_list').empty();
                var posts = response.responseJSON;
                for (var i = 0; i < posts.length; i++) {
                    var row = "";
                    row = row + "<p>" + '<img src="' + posts[i].photourl + '">' + "</p>";
                    row = row + "<p id=\"desc\">" + posts[i].posttext + '</p><div id="showComBtn' + posts[i].id + '" class="collapsible-menu"><input type="checkbox" style="display: none" id="menu' + posts[i].id + '"><label style="background-color: black; opacity: 0.7; color: white; position: relative; left: 160px; width: 35%;" id="comTitle' + posts[i].id + '" for="menu' + posts[i].id + '">Show comments</label><div class="menu-content">';

                    for (var j = 0; j < posts[i].comments.length; j++) {
                        row = row + "<p id=\"coms\">" + posts[i].comments[j].commenttext + "</p>";
                    }


                    row = row + '<form id="ComForm' + posts[i].id + '" name="CommentForm' + posts[i].id + '">'
                        + '<textarea name="commenttext" id="commenttext" style="font-family:sans-serif;font-size:1.2em;" placeholder="Tell us what you think!"></textarea>'
                        + '<input type="hidden" id="pId" name="postId" value="' + posts[i].id + '" />'
                        + '<input id="submitbtn" type="submit" value="Submit" onclick="saveComment(' + posts[i].id + ')" /></form></div>';

                    $('#post_list').append(row);
                }
            }
        }
    );
}


function giveMeComments(postId) {
    $.ajax(
        {
            method: "GET",
            url: "/Comments/" + postId,
            dataType: "json",
            contentType: "application/json",
            complete: function (response) {
                return response.responseJSON;
            }
        }
    );
}

function saveComment(postId) {
    $.ajax(
        {
            method: "POST",
            url: "/saveCom",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(getData($('form[name=CommentForm' + postId + ']').serializeArray())),
            complete: function (response) {
                giveMeAllPosts();
            }
        }
    );
}

function getData(data) {
    var result = {};
    $.map(data, function (n) {
        result[n['name']] = n['value'];
    });
    return result;
}


  var coll = document.getElementsByClassName("collapsible");
  var i;

  for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var content = this.nextElementSibling;
      if (content.style.maxHeight){
        content.style.maxHeight = null;
      } else {
        content.style.maxHeight = content.scrollHeight + "px";
      }
    });
  }