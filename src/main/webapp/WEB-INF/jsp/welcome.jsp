<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <link href="../static/css/style.css" th:href="@{/css/style.css}" rel="stylesheet" />
    <script type="text/javascript" th:src="@{/js/jquery.min.js}"></script>
    <script type="text/javascript" th:src="@{/js/jquery-ui.min.js}"></script>

    <meta charset="UTF-8">
    <title>The Right Camp</title>

</head>
<body>

<!--NAVIGATION BAR-->

<ul id="navbar">
    <!--<li><a href="home">Home</a></li>-->
    <li><a href="home">The Right Camp</a></li>
    <!--<li><a href="content">Content</a></li>-->
    <li><a href="userpage">User Page</a></li>
    <li style="float:right"><a class="active" href="login">Login</a></li>
    <li style="float:right"><a class="active" href="registration">Registration</a></li>
</ul>


<h2>Message: ${message}</h2>


<h1 id="intro">Welcome to the Right camp!</h1>

<p>A place where you can share amazing sites with your fellow travelers!</p>

</body>
</html>