package theRightCamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import theRightCamp.domain.Post;
import theRightCamp.repos.PostRepo;

@Controller
public class MainController {

    @Autowired UserService userService;

    @Autowired
    PostRepo postRepo;

    @RequestMapping("/content")
        public String getContent(){
        return "content";
    }

    @GetMapping("/home")
    public String getHome(){
        return "home";
    }

    @GetMapping("/")
    public String getHomeAgain(){
        return "home";
    }

    @GetMapping("/userpage")
    public String getUserpage(){
        return "userpage";
    }

    @GetMapping(value = "/specific/{id}")
    public String getSpecific(@PathVariable Long id, Model model){
        Post post = postRepo.getOne(id);
        model.addAttribute("post", post );
       return "specific";
   }
}