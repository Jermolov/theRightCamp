package theRightCamp.repos;

import org.springframework.data.repository.CrudRepository;
import theRightCamp.domain.Users;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface UserRepo extends CrudRepository <Users, Long>{

    List<Users> findByUsername(String username);
}
