package theRightCamp.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import theRightCamp.domain.Post;

import javax.transaction.Transactional;

@Transactional
public interface PostRepo extends JpaRepository<Post, Long>{

//    List<Post> findByphotourl(String photourl, String posttext);
}
