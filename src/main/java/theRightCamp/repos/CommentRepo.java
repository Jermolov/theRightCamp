package theRightCamp.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import theRightCamp.domain.Comment;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface CommentRepo extends JpaRepository <Comment, Long> {

    @Query("SELECT commenttext FROM Comment WHERE postId = :postId")
    List<Comment> findByPostId(@Param("postId") Long postId);

}
