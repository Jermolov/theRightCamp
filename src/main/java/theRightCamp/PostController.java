package theRightCamp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import theRightCamp.domain.Post;
import theRightCamp.repos.CommentRepo;
import theRightCamp.repos.PostRepo;

import javax.transaction.Transactional;
import java.util.List;


@RestController
public class PostController {

    @Autowired
    PostRepo postRepo;

    @GetMapping("/getPost")
    public String getPost() {
        return "specific";
    }

    @Transactional
    @PostMapping("/savePost")
    public String save(@RequestBody Post post) {
        Post dbPost = new Post();
        dbPost.setPosttext(post.getPosttext());
        dbPost.setPhotourl(post.getPhotourl());
        postRepo.save(dbPost);
        return "content";
    }

    @GetMapping(value = "/list", produces = "application/json")
    public List<Post> list() { return postRepo.findAll();
    }
}