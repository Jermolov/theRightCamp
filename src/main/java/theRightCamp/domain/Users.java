package theRightCamp.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "users")

public class Users extends BaseEntity  {

    private String username;
    private String password;
    private Long user_id;

    public Users() {
    }

    public Users(String username, String password){
        this.password = password;
        this.username = username;
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "id")
    private List<Post> posts;
}

