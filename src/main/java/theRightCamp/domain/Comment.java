package theRightCamp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "comments")
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Comment extends BaseEntity {

    @Column(name="post_id")
    private Long postId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name="commenttext")
    private String commenttext;

    public Comment(){
    }

    public Comment(String commenttext) {
        this.commenttext = commenttext;
    }

}
