package theRightCamp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "posts")
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Post extends BaseEntity {

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "posttext")
    private String posttext;

    @Column(name = "photourl")
    private String photourl;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "post_id")
    @OrderBy("post_id asc")
    private List<Comment> comments;

    public Post() {
    }

    public Post(String posttext, String photourl, Long userId, Long id) {
        this.photourl = photourl;
        this.posttext = posttext;
        this.userId = userId;

    }

}
