package theRightCamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import theRightCamp.domain.Comment;
import theRightCamp.repos.CommentRepo;
import theRightCamp.repos.PostRepo;

import javax.transaction.Transactional;
import java.util.List;

@RestController
public class CommentController {

    @Autowired
    CommentRepo commentRepo;

    @Autowired
    PostRepo postRepo;

    @Transactional
    @PostMapping("/saveCom")
    public void saveComment(@RequestBody Comment comment) {
        Comment dbComment = new Comment();
        dbComment.setCommenttext(comment.getCommenttext());
        dbComment.setPostId(comment.getPostId());
        commentRepo.save(dbComment);
    }

    @GetMapping(value = "/Comments/{postId}", produces = "application/json" )
    @ResponseBody
    public List<Comment> commentList(@PathVariable Long postId) {
        return commentRepo.findByPostId(postId);
    }
}
