package theRightCamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import theRightCamp.domain.Post;
import theRightCamp.repos.CommentRepo;
import theRightCamp.repos.PostRepo;
import theRightCamp.repos.UserRepo;

import javax.transaction.Transactional;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PostRepo postRepo;

    @Autowired
    private CommentRepo commentRepo;

    public Post getPost(Long id) {
        return postRepo.getOne(id);
    }

    @Transactional
    public void savePost(Post post) {
        Post dbPost = postRepo.save(post);
    }
}
