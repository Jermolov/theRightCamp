# theRightCamp

This is a social academic project. 
The main purpose of this project is to give users opportunities to share camping places that are right (best) for them, 
as well as the opportunity to argue posts from other users.

User case: Main idea is to make app, where user is able to share interesting travel locations for camping. 
After registration and signing in, the user can add own travel posts, review or search other peoples posts. 
Post consists of: picture, text description with max length of 1000 characters and mandatory geolocation. 
Also users can add comments to every post and edit, delete own comments