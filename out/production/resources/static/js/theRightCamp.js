$(document).ready(function () {
        giveMeAllPosts();
        //giveMeComments();
    }
);

function giveMeAllPosts() {
    $.ajax(
        {
            method: "GET",
            url: "/list",
            dataType: "json",
            contentType: "application/json",
            complete: function (response) {

                $('#post_list').empty();
                var posts = response.responseJSON;
                for (var i = 0; i < posts.length; i++) {
                    var row = "";
                    row = row + "<p>" + '<img src="' + posts[i].photourl + '">' + "</p>";
                    row = row + "<p>" + posts[i].posttext + "</p>";

                    for (var j = 0; j < posts[i].comments.length; j++) {
                        row = row + "<p>" + posts[i].comments[j].commenttext + "</p>";
                    }


                    row = row + '<form id="ComForm' + posts[i].id + '" name="CommentForm' + posts[i].id + '">'
                        + '<textarea name="commenttext" id="commenttext" style="font-family:sans-serif;font-size:1.2em;" placeholder="Tell us what you think!"></textarea>'
                        + '<input type="hidden" id="postId" name="postId" value="' + posts[i].id + '" />'
                        + '<input type="submit" value="Submit" onclick="saveComment(' + posts[i].id + ')" /></form>';


                    // console.log("This is the post id laura tuli" + posts[i].id);
                    // var comments = giveMeComments(posts[i].id);
                    // console.log("show me ur comments" + posts[i].id);
                    // for (var j = 0; j < comments.length; j++) {
                    //     row = row + "<p>" + comments[j].commenttext + "</p>";
                    // }

                    console.log("After comment iteration" + posts[i].id);

                    $('#post_list').append(row);
                }
            }
        }
    );
}


function giveMeComments(postId) {
    $.ajax(
        {
            method: "GET",
            url: "/Comments/" + postId,
            dataType: "json",
            contentType: "application/json",
            complete: function (response) {
                return response.responseJSON;
            }
        }
    );
}

function saveComment(postId) {
    $.ajax(
        {
            method: "POST",
            url: "/saveCom",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(getData($('form[name=CommentForm' + postId + ']').serializeArray())),
            complete: function (response) {
                giveMeAllPosts();
                <!--giveMeAllComments();-->
            }
        }
    );
}

function getData(data) {
    var result = {};
    $.map(data, function (n) {
        result[n['name']] = n['value'];
    });
    return result;
}